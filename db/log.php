<?php

defined('MOODLE_INTERNAL') || die();

$logs = array(
    array('module'=>'openaire', 'action'=>'view', 'mtable'=>'openaire', 'field'=>'name'),
    array('module'=>'openaire', 'action'=>'view all', 'mtable'=>'openaire', 'field'=>'name'),
    array('module'=>'openaire', 'action'=>'update', 'mtable'=>'openaire', 'field'=>'name'),
    array('module'=>'openaire', 'action'=>'add', 'mtable'=>'openaire', 'field'=>'name'),
);