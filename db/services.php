<?php

defined('MOODLE_INTERNAL') || die;

$functions = array(

    'mod_openaire_view_openaire' => array(
        'classname'     => 'mod_openaire_external',
        'methodname'    => 'view_openaire',
        'description'   => 'Trigger the course module viewed event and update the module completion status.',
        'type'          => 'write',
        'capabilities'  => 'mod/openaire:view',
        'services'      => array(MOODLE_OFFICIAL_MOBILE_SERVICE)
    ),
    'mod_openaire_get_openaires_by_courses' => array(
        'classname'     => 'mod_openaire_external',
        'methodname'    => 'get_openaires_by_courses',
        'description'   => 'Returns a list of openaires in a provided list of courses, if no list is provided all openaires that the user
                            can view will be returned.',
        'type'          => 'read',
        'capabilities'  => 'mod/openaire:view',
        'services'      => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
);
