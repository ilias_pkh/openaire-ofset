<?php

defined('MOODLE_INTERNAL') || die;

require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/openaire/locallib.php');

class mod_openaire_mod_form extends moodleform_mod {
    function definition() {
        global $PAGE, $CFG, $DB;
        $mform = $this->_form;

        $config = get_config('openaire');

        //-------------------------------------------------------
        
        $mform->addElement('header', 'general', get_string('general', 'form'));

        

        $mform->addElement('text', 'name', get_string('name'), array('size'=>'48'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        
        //$mform->addElement('url', 'externalurl', get_string('externalurl', 'url'), array('size'=>'60'), array('usefilepicker'=>true));
        //$mform->setType('externalurl', PARAM_RAW_TRIMMED);
        //$mform->addRule('externalurl', null, 'required', null, 'client');

        $this->standard_intro_elements();
        $element = $mform->getElement('introeditor');
        $attributes = $element->getAttributes();
        $attributes['rows'] = 5;
        $element->setAttributes($attributes);

        
        //-------------------------------------------------------
        /*
        $mform->addElement('header', 'optionssection', get_string('appearance'));

        if ($this->current->instance) {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions), $this->current->display);
        } else {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions));
        }
        if (count($options) == 1) {
            $mform->addElement('hidden', 'display');
            $mform->setType('display', PARAM_INT);
            reset($options);
            $mform->setDefault('display', key($options));
        } else {
            $mform->addElement('select', 'display', get_string('displayselect', 'url'), $options);
            $mform->setDefault('display', $config->display);
            $mform->addHelpButton('display', 'displayselect', 'url');
        }

        if (array_key_exists(RESOURCELIB_DISPLAY_POPUP, $options)) {
            $mform->addElement('text', 'popupwidth', get_string('popupwidth', 'url'), array('size'=>3));
            if (count($options) > 1) {
                $mform->hideIf('popupwidth', 'display', 'noteq', RESOURCELIB_DISPLAY_POPUP);
            }
            $mform->setType('popupwidth', PARAM_INT);
            $mform->setDefault('popupwidth', $config->popupwidth);

            $mform->addElement('text', 'popupheight', get_string('popupheight', 'openaire'), array('size'=>3));
            if (count($options) > 1) {
                $mform->hideIf('popupheight', 'display', 'noteq', RESOURCELIB_DISPLAY_POPUP);
            }
            $mform->setType('popupheight', PARAM_INT);
            $mform->setDefault('popupheight', $config->popupheight);
        }

        if (array_key_exists(RESOURCELIB_DISPLAY_AUTO, $options) or
          array_key_exists(RESOURCELIB_DISPLAY_EMBED, $options) or
          array_key_exists(RESOURCELIB_DISPLAY_FRAME, $options)) {
            $mform->addElement('checkbox', 'printintro', get_string('printintro', 'openaire'));
            $mform->hideIf('printintro', 'display', 'eq', RESOURCELIB_DISPLAY_POPUP);
            $mform->hideIf('printintro', 'display', 'eq', RESOURCELIB_DISPLAY_OPEN);
            $mform->hideIf('printintro', 'display', 'eq', RESOURCELIB_DISPLAY_NEW);
            $mform->setDefault('printintro', $config->printintro);
        }

        */

        //-------------------------------------------------------

        /*

        $mform->addElement('header', 'parameterssection', get_string('parametersheader', 'url'));
        $mform->addElement('static', 'parametersinfo', '', get_string('parametersheader_help', 'url'));

        if (empty($this->current->parameters)) {
            $parcount = 5;
        } else {
            $parcount = 5 + count(unserialize($this->current->parameters));
            $parcount = ($parcount > 100) ? 100 : $parcount;
        }
        $options = openaire_get_variable_options($config);

        for ($i=0; $i < $parcount; $i++) {
            $parameter = "parameter_$i";
            $variable  = "variable_$i";
            $pargroup = "pargoup_$i";
            $group = array(
                $mform->createElement('text', $parameter, '', array('size'=>'12')),
                $mform->createElement('selectgroups', $variable, '', $options),
            );
            $mform->addGroup($group, $pargroup, get_string('parameterinfo', 'openaire'), ' ', false);
            $mform->setType($parameter, PARAM_RAW);
        }

        */

        //-------------------------------------------------------

        $mform->addElement('header', 'advancedSearch', "Search for research outcomes");

        $types = array(
            "publications"  => "Publications",
            "datasets"  => "Research Data",
            "software" => "Software",
            "other" => "Other research products",
        );

        $mform->addElement('select', 'type', "Entity Type", $types);

        $mform->addElement('text', 'keywords', "Publication Keywords", array('size'=>'48'));
        $mform->setType('keywords', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('keywords', '');
        $mform->addRule('keywords', null, 'required', null, 'client');

        $mform->addElement('text', 'title', "Title", array('size'=>'48'));
        $mform->setType('title', PARAM_NOTAGS);                   
        $mform->setDefault('title', '');

        $mform->addElement('text', 'author', "Author", array('size'=>'48'));
        $mform->setType('author', PARAM_NOTAGS);                   
        $mform->setDefault('author', '');

        $mform->addElement('text', 'fp7scientificarea', "FP7 Scientific Area", array('size'=>'48'));
        $mform->setType('fp7', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('fp7', '');

        $mform->addElement('checkbox', 'acceptedDate', "Accepted Date");

        $mform->addElement('date_selector', 'fromDateAccepted', "From", array('size'=>'48'));
        $mform->setType('fromDateAccepted', PARAM_NOTAGS);                   
        $mform->setDefault('fromDateAccepted', '');

        $mform->addElement('date_selector', 'toDateAccepted', "To", array('size'=>'48'));
        $mform->setType('toDateAccepted', PARAM_NOTAGS);                   
        $mform->setDefault('toDateAccepted', '');


        $mform->addElement('button', 'searchOpenaire', "Search OpenAIRE");
        $PAGE->requires->js('/mod/openaire/script.js');
        // $PAGE->requires->js( new moodle_url($CFG->wwwroot . '/mod/openaire/script.js') );
        // $mform->addElement('script', 'asdf', "var j = 2;");

        //-------------------------------------------------------
        $this->standard_coursemodule_elements();

        //-------------------------------------------------------
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values) {
        if (!empty($default_values['displayoptions'])) {
            $displayoptions = unserialize($default_values['displayoptions']);
            if (isset($displayoptions['printintro'])) {
                $default_values['printintro'] = $displayoptions['printintro'];
            }
            if (!empty($displayoptions['popupwidth'])) {
                $default_values['popupwidth'] = $displayoptions['popupwidth'];
            }
            if (!empty($displayoptions['popupheight'])) {
                $default_values['popupheight'] = $displayoptions['popupheight'];
            }
        }
        if (!empty($default_values['parameters'])) {
            $parameters = unserialize($default_values['parameters']);
            $i = 0;
            foreach ($parameters as $parameter=>$variable) {
                $default_values['parameter_'.$i] = $parameter;
                $default_values['variable_'.$i]  = $variable;
                $i++;
            }
        }
    }

    function validation($data, $files) {
        //$errors = parent::validation($data, $files);

        // Validating Entered openaire, we are looking for obvious problems only,
        // teachers are responsible for testing if it actually works.

        // This is not a security validation!! Teachers are allowed to enter "javascript:alert(666)" for example.

        // NOTE: do not try to explain the difference between URL and URI, people would be only confused...

        // if (!empty($data['externalurl'])) {
        //     $openaire = $data['externalurl'];
        //     if (preg_match('|^/|', $openaire)) {
        //         // links relative to server root are ok - no validation necessary

        //     } else if (preg_match('|^[a-z]+://|i', $openaire) or preg_match('|^https?:|i', $openaire) or preg_match('|^ftp:|i', $openaire)) {
        //         // normal URL
        //         if (!openaire_appears_valid_url($openaire)) {
        //             $errors['externalurl'] = get_string('invalidurl', 'url');
        //         }

        //     } else if (preg_match('|^[a-z]+:|i', $openaire)) {
        //         // general URI such as teamspeak, mailto, etc. - it may or may not work in all browsers,
        //         // we do not validate these at all, sorry

        //     } else {
        //         // invalid URI, we try to fix it by adding 'http://' prefix,
        //         // relative links are NOT allowed because we display the link on different pages!
        //         if (!openaire_appears_valid_url('http://'.$openaire)) {
        //             $errors['externalurl'] = get_string('invalidurl', 'url');
        //         }
        //     }
        // }
        return $errors;
    }

}
