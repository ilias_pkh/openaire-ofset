<?php

require('../../config.php');
require_once("$CFG->dirroot/mod/openaire/lib.php");
require_once("$CFG->dirroot/mod/openaire/locallib.php");
require_once($CFG->libdir . '/completionlib.php');

$id       = optional_param('id', 0, PARAM_INT);        // Course module ID
$u        = optional_param('u', 0, PARAM_INT);         // openaire instance id
$redirect = optional_param('redirect', 0, PARAM_BOOL);
$forceview = optional_param('forceview', 1, PARAM_BOOL);

if ($u) {  // Two ways to specify the module
    $openaire = $DB->get_record('openaire', array('id'=>$u), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('openaire', $openaire->id, $openaire->course, false, MUST_EXIST);

} else {
    $cm = get_coursemodule_from_id('openaire', $id, 0, false, MUST_EXIST);
    $openaire = $DB->get_record('openaire', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/openaire:view', $context);

// Completion and trigger events.
openaire_view($openaire, $course, $cm, $context);

$PAGE->set_url('/mod/openaire/view.php', array('id' => $cm->id));

// Make sure openaire exists before generating output - some older sites may contain empty openaires
// Do not use PARAM_openaire here, it is too strict and does not support general URIs!

//$exturl = trim($openaire->externalurl);
$exturl = trim('https://explore.openaire.eu/search/publication?articleId=dedup_wf_001::f883f904becc96173949c53f08e35c0a');
if (empty($exturl) or $exturl === 'http://') {
    url_print_header($url, $cm, $course);
    url_print_heading($url, $cm, $course);
    url_print_intro($url, $cm, $course);
    notice(get_string('invalidstoredurl', 'url'), new moodle_url('/course/view.php', array('id'=>$cm->course)));
    die;
}
unset($exturl);


$displaytype = openaire_get_final_display_type($openaire);
if ($displaytype == RESOURCELIB_DISPLAY_OPEN) {
    $redirect = true;
}

if ($redirect && !$forceview) {
    // coming from course page or openaire index page,
    // the redirection is needed for completion tracking and logging
    $fullopenaire = str_replace('&amp;', '&', openaire_get_full_url($openaire, $cm, $course));

    if (!course_get_format($course)->has_view_page()) {
        // If course format does not have a view page, add redirection delay with a link to the edit page.
        // Otherwise teacher is redirected to the external openaire without any possibility to edit activity or course settings.
        $editopenaire = null;
        if (has_capability('moodle/course:manageactivities', $context)) {
            $editopenaire = new moodle_openaire('/course/modedit.php', array('update' => $cm->id));
            $edittext = get_string('editthisactivity');
        } else if (has_capability('moodle/course:update', $context->get_course_context())) {
            $editopenaire = new moodle_openaire('/course/edit.php', array('id' => $course->id));
            $edittext = get_string('editcoursesettings');
        }
        if ($editopenaire) {
            redirect($fullopenaire, html_writer::link($editopenaire, $edittext)."<br/>".
                    get_string('pageshouldredirect'), 10);
        }
    }
    redirect($fullopenaire);
}

switch ($displaytype) {
    case RESOURCELIB_DISPLAY_EMBED:
        openaire_display_embed($openaire, $cm, $course);
        break;
    case RESOURCELIB_DISPLAY_FRAME:
        openaire_display_frame($openaire, $cm, $course);
        break;
    default:
        openaire_print_workaround($openaire, $cm, $course);
        break;
}
