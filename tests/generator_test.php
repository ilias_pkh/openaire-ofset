<?php

class mod_openaire_generator_testcase extends advanced_testcase {

    public function test_create_instance() {
        global $DB;
        $this->resetAfterTest();
        $this->setAdminUser();

        $course = $this->getDataGenerator()->create_course();

        $this->assertFalse($DB->record_exists('openaire', array('course' => $course->id)));
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course));
        $records = $DB->get_records('openaire', array('course' => $course->id), 'id');
        $this->assertEquals(1, count($records));
        $this->assertTrue(array_key_exists($openaire->id, $records));

        $params = array('course' => $course->id, 'name' => 'Another openaire');
        $openaire = $this->getDataGenerator()->create_module('openaire', $params);
        $records = $DB->get_records('openaire', array('course' => $course->id), 'id');
        $this->assertEquals(2, count($records));
        $this->assertEquals('Another openaire', $records[$openaire->id]->name);
    }
}
