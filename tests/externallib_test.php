<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot . '/webservice/tests/helpers.php');

/**
 * External mod_openaire functions unit tests
 *
 * @package    mod_openaire
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */
class mod_openaire_external_testcase extends externallib_advanced_testcase {

    /**
     * Test view_openaire
     */
    public function test_view_openaire() {
        global $DB;

        $this->resetAfterTest(true);

        // Setup test data.
        $course = $this->getDataGenerator()->create_course();
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course->id));
        $context = context_module::instance($openaire->cmid);
        $cm = get_coursemodule_from_instance('openaire', $openaire->id);

        // Test invalid instance id.
        try {
            mod_openaire_external::view_openaire(0);
            $this->fail('Exception expected due to invalid mod_openaire instance id.');
        } catch (moodle_exception $e) {
            $this->assertEquals('invalidrecord', $e->errorcode);
        }

        // Test not-enrolled user.
        $user = self::getDataGenerator()->create_user();
        $this->setUser($user);
        try {
            mod_openaire_external::view_openaire($openaire->id);
            $this->fail('Exception expected due to not enrolled user.');
        } catch (moodle_exception $e) {
            $this->assertEquals('requireloginerror', $e->errorcode);
        }

        // Test user with full capabilities.
        $studentrole = $DB->get_record('role', array('shortname' => 'student'));
        $this->getDataGenerator()->enrol_user($user->id, $course->id, $studentrole->id);

        // Trigger and capture the event.
        $sink = $this->redirectEvents();

        $result = mod_openaire_external::view_openaire($openaire->id);
        $result = external_api::clean_returnvalue(mod_openaire_external::view_openaire_returns(), $result);

        $events = $sink->get_events();
        $this->assertCount(1, $events);
        $event = array_shift($events);

        // Checking that the event contains the expected values.
        $this->assertInstanceOf('\mod_openaire\event\course_module_viewed', $event);
        $this->assertEquals($context, $event->get_context());
        $moodleopenaire = new \moodle_openaire('/mod/openaire/view.php', array('id' => $cm->id));
        $this->assertEquals($moodleopenaire, $event->get_openaire());
        $this->assertEventContextNotUsed($event);
        $this->assertNotEmpty($event->get_name());

        // Test user with no capabilities.
        // We need a explicit prohibit since this capability is only defined in authenticated user and guest roles.
        assign_capability('mod/openaire:view', CAP_PROHIBIT, $studentrole->id, $context->id);
        // Empty all the caches that may be affected by this change.
        accesslib_clear_all_caches_for_unit_testing();
        course_modinfo::clear_instance_cache();

        try {
            mod_openaire_external::view_openaire($openaire->id);
            $this->fail('Exception expected due to missing capability.');
        } catch (moodle_exception $e) {
            $this->assertEquals('requireloginerror', $e->errorcode);
        }

    }

    /**
     * Test test_mod_openaire_get_openaires_by_courses
     */
    public function test_mod_openaire_get_openaires_by_courses() {
        global $DB;

        $this->resetAfterTest(true);

        $course1 = self::getDataGenerator()->create_course();
        $course2 = self::getDataGenerator()->create_course();

        $student = self::getDataGenerator()->create_user();
        $studentrole = $DB->get_record('role', array('shortname' => 'student'));
        $this->getDataGenerator()->enrol_user($student->id, $course1->id, $studentrole->id);

        // First openaire.
        $record = new stdClass();
        $record->course = $course1->id;
        $openaire1 = self::getDataGenerator()->create_module('openaire', $record);

        // Second openaire.
        $record = new stdClass();
        $record->course = $course2->id;
        $openaire = self::getDataGenerator()->create_module('openaire', $record);

        // Execute real Moodle enrolment as we'll call unenrol() method on the instance later.
        $enrol = enrol_get_plugin('manual');
        $enrolinstances = enrol_get_instances($course2->id, true);
        foreach ($enrolinstances as $courseenrolinstance) {
            if ($courseenrolinstance->enrol == "manual") {
                $instance2 = $courseenrolinstance;
                break;
            }
        }
        $enrol->enrol_user($instance2, $student->id, $studentrole->id);

        self::setUser($student);

        $returndescription = mod_openaire_external::get_openaires_by_courses_returns();

        // Create what we expect to be returned when querying the two courses.
        $expectedfields = array('id', 'coursemodule', 'course', 'name', 'intro', 'introformat', 'introfiles', 'externalurl',
                                'display', 'displayoptions', 'parameters', 'timemodified', 'section', 'visible', 'groupmode',
                                'groupingid');

        // Add expected coursemodule and data.
        $openaire1->coursemodule = $openaire1->cmid;
        $openaire1->introformat = 1;
        $openaire1->section = 0;
        $openaire1->visible = true;
        $openaire1->groupmode = 0;
        $openaire1->groupingid = 0;
        $openaire1->introfiles = [];

        $openaire->coursemodule = $openaire->cmid;
        $openaire->introformat = 1;
        $openaire->section = 0;
        $openaire->visible = true;
        $openaire->groupmode = 0;
        $openaire->groupingid = 0;
        $openaire->introfiles = [];

        foreach ($expectedfields as $field) {
            $expected1[$field] = $openaire1->{$field};
            $expected2[$field] = $openaire->{$field};
        }

        $expectedopenaires = array($expected2, $expected1);

        // Call the external function passing course ids.
        $result = mod_openaire_external::get_openaires_by_courses(array($course2->id, $course1->id));
        $result = external_api::clean_returnvalue($returndescription, $result);

        $this->assertEquals($expectedopenaires, $result['openaires']);
        $this->assertCount(0, $result['warnings']);

        // Call the external function without passing course id.
        $result = mod_openaire_external::get_openaires_by_courses();
        $result = external_api::clean_returnvalue($returndescription, $result);
        $this->assertEquals($expectedopenaires, $result['openaires']);
        $this->assertCount(0, $result['warnings']);

        // Add a file to the intro.
        $filename = "file.txt";
        $filerecordinline = array(
            'contextid' => context_module::instance($openaire->cmid)->id,
            'component' => 'mod_openaire',
            'filearea'  => 'intro',
            'itemid'    => 0,
            'filepath'  => '/',
            'filename'  => $filename,
        );
        $fs = get_file_storage();
        $timepost = time();
        $fs->create_file_from_string($filerecordinline, 'image contents (not really)');

        $result = mod_openaire_external::get_openaires_by_courses(array($course2->id, $course1->id));
        $result = external_api::clean_returnvalue($returndescription, $result);

        $this->assertCount(1, $result['openaires'][0]['introfiles']);
        $this->assertEquals($filename, $result['openaires'][0]['introfiles'][0]['filename']);

        // Unenrol user from second course and alter expected openaires.
        $enrol->unenrol_user($instance2, $student->id);
        array_shift($expectedopenaires);

        // Call the external function without passing course id.
        $result = mod_openaire_external::get_openaires_by_courses();
        $result = external_api::clean_returnvalue($returndescription, $result);
        $this->assertEquals($expectedopenaires, $result['openaires']);

        // Call for the second course we unenrolled the user from, expected warning.
        $result = mod_openaire_external::get_openaires_by_courses(array($course2->id));
        $this->assertCount(1, $result['warnings']);
        $this->assertEquals('1', $result['warnings'][0]['warningcode']);
        $this->assertEquals($course2->id, $result['warnings'][0]['itemid']);
    }
}
