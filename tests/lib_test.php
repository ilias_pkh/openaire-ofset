<?php

defined('MOODLE_INTERNAL') || die();


/**
 * mod_openaire tests
 *
 * @package    mod_openaire
 * @category   phpunit
 * @copyright  2011 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_openaire_lib_testcase extends advanced_testcase {

    /**
     * Prepares things before this test case is initialised
     * @return void
     */
    public static function setUpBeforeClass(): void {
        global $CFG;
        require_once($CFG->dirroot . '/mod/openaire/lib.php');
        require_once($CFG->dirroot . '/mod/openaire/locallib.php');
    }

    /**
     * Tests the openaire_appears_valid_openaire function
     * @return void
     */
    public function test_openaire_appears_valid_openaire() {
        $this->assertTrue(openaire_appears_valid_openaire('http://example'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.examplé.com'));
        $this->assertTrue(openaire_appears_valid_openaire('http://💩.la'));
        $this->assertTrue(openaire_appears_valid_openaire('http://香港大學.香港'));
        $this->assertTrue(openaire_appears_valid_openaire('http://وزارة-الأتصالات.مصر'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.теннис-алт.рф'));
        $this->assertTrue(openaire_appears_valid_openaire('http://имена.бг'));
        $this->assertTrue(openaire_appears_valid_openaire('http://straße.de'));
        $this->assertTrue(openaire_appears_valid_openaire('http://キース.コム'));
        $this->assertTrue(openaire_appears_valid_openaire('http://太亞.中国'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.რეგისტრაცია.გე'));
        $this->assertTrue(openaire_appears_valid_openaire('http://уміц.укр'));
        $this->assertTrue(openaire_appears_valid_openaire('http://현대.한국'));
        $this->assertTrue(openaire_appears_valid_openaire('http://мон.мон'));
        $this->assertTrue(openaire_appears_valid_openaire('http://тест.қаз'));
        $this->assertTrue(openaire_appears_valid_openaire('http://рнидс.срб'));
        $this->assertTrue(openaire_appears_valid_openaire('http://اسماء.شبكة'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.informationssäkerhet.se'));
        $this->assertTrue(openaire_appears_valid_openaire('http://москва.рф/services'));
        $this->assertTrue(openaire_appears_valid_openaire('http://detdumærker.dk'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.exa-mple2.com'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com/~nobody/index.html'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com#hmm'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com/#hmm'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com/žlutý koníček/lala.txt'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com/žlutý koníček/lala.txt#hmmmm'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com/index.php?xx=yy&zz=aa'));
        $this->assertTrue(openaire_appears_valid_openaire('http://www.example.com:80/index.php?xx=yy&zz=aa'));
        $this->assertTrue(openaire_appears_valid_openaire('https://user:password@www.example.com/žlutý koníček/lala.txt'));
        $this->assertTrue(openaire_appears_valid_openaire('ftp://user:password@www.example.com/žlutý koníček/lala.txt'));

        $this->assertFalse(openaire_appears_valid_openaire('http:example.com'));
        $this->assertFalse(openaire_appears_valid_openaire('http:/example.com'));
        $this->assertFalse(openaire_appears_valid_openaire('http://'));
        $this->assertFalse(openaire_appears_valid_openaire('http://www.exa mple.com'));
        $this->assertFalse(openaire_appears_valid_openaire('http://@www.example.com'));
        $this->assertFalse(openaire_appears_valid_openaire('http://user:@www.example.com'));

        $this->assertTrue(openaire_appears_valid_openaire('lalala://@:@/'));
    }

    /**
     * Test openaire_view
     * @return void
     */
    public function test_openaire_view() {
        global $CFG;

        $CFG->enablecompletion = 1;
        $this->resetAfterTest();

        // Setup test data.
        $course = $this->getDataGenerator()->create_course(array('enablecompletion' => 1));
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course->id),
                                                            array('completion' => 2, 'completionview' => 1));
        $context = context_module::instance($openaire->cmid);
        $cm = get_coursemodule_from_instance('openaire', $openaire->id);

        // Trigger and capture the event.
        $sink = $this->redirectEvents();

        $this->setAdminUser();
        openaire_view($openaire, $course, $cm, $context);

        $events = $sink->get_events();
        // 2 additional events thanks to completion.
        $this->assertCount(3, $events);
        $event = array_shift($events);

        // Checking that the event contains the expected values.
        $this->assertInstanceOf('\mod_openaire\event\course_module_viewed', $event);
        $this->assertEquals($context, $event->get_context());
        $openaire = new \moodle_openaire('/mod/openaire/view.php', array('id' => $cm->id));
        $this->assertEquals($openaire, $event->get_openaire());
        $this->assertEventContextNotUsed($event);
        $this->assertNotEmpty($event->get_name());

        // Check completion status.
        $completion = new completion_info($course);
        $completiondata = $completion->get_data($cm);
        $this->assertEquals(1, $completiondata->completionstate);
    }

    /**
     * Test mod_openaire_core_calendar_provide_event_action with user override
     */
    public function test_openaire_core_calendar_provide_event_action_user_override() {
        global $CFG, $USER;

        $this->resetAfterTest();
        $this->setAdminUser();
        $user = $this->getDataGenerator()->create_user();
        $CFG->enablecompletion = 1;

        // Create the activity.
        $course = $this->getDataGenerator()->create_course(array('enablecompletion' => 1));
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course->id),
            array('completion' => 2, 'completionview' => 1, 'completionexpected' => time() + DAYSECS));

        // Get some additional data.
        $cm = get_coursemodule_from_instance('openaire', $openaire->id);

        // Create a calendar event.
        $event = $this->create_action_event($course->id, $openaire->id,
            \core_completion\api::COMPLETION_EVENT_TYPE_DATE_COMPLETION_EXPECTED);

        // Mark the activity as completed.
        $completion = new completion_info($course);
        $completion->set_module_viewed($cm);

        // Create an action factory.
        $factory = new \core_calendar\action_factory();

        // Decorate action event.
        $actionevent = mod_openaire_core_calendar_provide_event_action($event, $factory, $USER->id);

        // Decorate action with a userid override.
        $actionevent2 = mod_openaire_core_calendar_provide_event_action($event, $factory, $user->id);

        // Ensure result was null because it has been marked as completed for the associated user.
        // Logic was brought across from the "_already_completed" function.
        $this->assertNull($actionevent);

        // Confirm the event was decorated.
        $this->assertNotNull($actionevent2);
        $this->assertInstanceOf('\core_calendar\local\event\value_objects\action', $actionevent2);
        $this->assertEquals(get_string('view'), $actionevent2->get_name());
        $this->assertInstanceOf('moodle_openaire', $actionevent2->get_openaire());
        $this->assertEquals(1, $actionevent2->get_item_count());
        $this->assertTrue($actionevent2->is_actionable());
    }

    public function test_openaire_core_calendar_provide_event_action() {
        $this->resetAfterTest();
        $this->setAdminUser();

        // Create the activity.
        $course = $this->getDataGenerator()->create_course();
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course->id));

        // Create a calendar event.
        $event = $this->create_action_event($course->id, $openaire->id,
            \core_completion\api::COMPLETION_EVENT_TYPE_DATE_COMPLETION_EXPECTED);

        // Create an action factory.
        $factory = new \core_calendar\action_factory();

        // Decorate action event.
        $actionevent = mod_openaire_core_calendar_provide_event_action($event, $factory);

        // Confirm the event was decorated.
        $this->assertInstanceOf('\core_calendar\local\event\value_objects\action', $actionevent);
        $this->assertEquals(get_string('view'), $actionevent->get_name());
        $this->assertInstanceOf('moodle_openaire', $actionevent->get_openaire());
        $this->assertEquals(1, $actionevent->get_item_count());
        $this->assertTrue($actionevent->is_actionable());
    }

    public function test_openaire_core_calendar_provide_event_action_already_completed() {
        global $CFG;

        $this->resetAfterTest();
        $this->setAdminUser();

        $CFG->enablecompletion = 1;

        // Create the activity.
        $course = $this->getDataGenerator()->create_course(array('enablecompletion' => 1));
        $openaire = $this->getDataGenerator()->create_module('openaire', array('course' => $course->id),
            array('completion' => 2, 'completionview' => 1, 'completionexpected' => time() + DAYSECS));

        // Get some additional data.
        $cm = get_coursemodule_from_instance('openaire', $openaire->id);

        // Create a calendar event.
        $event = $this->create_action_event($course->id, $openaire->id,
            \core_completion\api::COMPLETION_EVENT_TYPE_DATE_COMPLETION_EXPECTED);

        // Mark the activity as completed.
        $completion = new completion_info($course);
        $completion->set_module_viewed($cm);

        // Create an action factory.
        $factory = new \core_calendar\action_factory();

        // Decorate action event.
        $actionevent = mod_openaire_core_calendar_provide_event_action($event, $factory);

        // Ensure result was null.
        $this->assertNull($actionevent);
    }

    /**
     * Creates an action event.
     *
     * @param int $courseid The course id.
     * @param int $instanceid The instance id.
     * @param string $eventtype The event type.
     * @return bool|calendar_event
     */
    private function create_action_event($courseid, $instanceid, $eventtype) {
        $event = new stdClass();
        $event->name = 'Calendar event';
        $event->modulename  = 'openaire';
        $event->courseid = $courseid;
        $event->instance = $instanceid;
        $event->type = CALENDAR_EVENT_TYPE_ACTION;
        $event->eventtype = $eventtype;
        $event->timestart = time();

        return calendar_event::create($event);
    }
}
