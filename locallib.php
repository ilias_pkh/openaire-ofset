<?php

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/filelib.php");
require_once("$CFG->libdir/resourcelib.php");
require_once("$CFG->dirroot/mod/openaire/lib.php");

/**
 * This methods does weak openaire validation, we are looking for major problems only,
 * no strict RFE validation.
 *
 * @param $url
 * @return bool true is seems valid, false if definitely not valid URL
 */
function openaire_appears_valid_url($url) {
    if (preg_match('/^(\/|https?:|ftp:)/i', $url)) {
        // note: this is not exact validation, we look for severely malformed URLs only
        return (bool) preg_match('/^[a-z]+:\/\/([^:@\s]+:[^@\s]+@)?[^ @]+(:[0-9]+)?(\/[^#]*)?(#.*)?$/i', $url);
    } else {
        return (bool)preg_match('/^[a-z]+:\/\/...*$/i', $url);
    }
}

/**
 * Fix common URL problems that we want teachers to see fixed
 * the next time they edit the resource.
 *
 * This function does not include any XSS protection.
 *
 * @param string $url
 * @return string
 */
function openaire_fix_submitted_url($openaire) {
    // note: empty openaires are prevented in form validation
    $openaire = trim($openaire);

    // remove encoded entities - we want the raw URI here
    $openaire = html_entity_decode($openaire, ENT_QUOTES, 'UTF-8');

    if (!preg_match('|^[a-z]+:|i', $openaire) and !preg_match('|^/|', $openaire)) {
        // invalid URI, try to fix it by making it normal URL,
        // please note relative openaires are not allowed, /xx/yy links are ok
        $openaire = 'http://'.$openaire;
    }

    return $openaire;
}

/**
 * Return full openaire with all extra parameters
 *
 * This function does not include any XSS protection.
 *
 * @param string $openaire
 * @param object $cm
 * @param object $course
 * @param object $config
 * @return string openaire with & encoded as &amp;
 */
function openaire_get_full_url($openaire, $cm, $course, $config=null) {
    //return $openaire;

    $parameters = empty($openaire->parameters) ? array() : unserialize($openaire->parameters);

    // make sure there are no encoded entities, it is ok to do this twice

 
    $manage = json_decode($openaire->externalurl, true);

    //$fullopenaire = html_entity_decode($openaire->externalurl, ENT_QUOTES, 'UTF-8');
    //$fullopenaire = html_entity_decode($manage[0]['url'], ENT_QUOTES, 'UTF-8');


    foreach ($manage as $openaireurl) {
        echo("<h3>" . $openaireurl['title'] . "</h3>");
        echo("<div class=\"urlworkaround\">Click <a href=\"" . $openaireurl['url'] . "\">" . $openaireurl['url'] . "</a> link to open resource.</div>");

    }
    
    

    $letters = '\pL';
    $latin = 'a-zA-Z';
    $digits = '0-9';
    $symbols = '\x{20E3}\x{00AE}\x{00A9}\x{203C}\x{2047}\x{2048}\x{2049}\x{3030}\x{303D}\x{2139}\x{2122}\x{3297}\x{3299}' .
               '\x{2300}-\x{23FF}\x{2600}-\x{27BF}\x{2B00}-\x{2BF0}';
    $arabic = '\x{FE00}-\x{FEFF}';
    $math = '\x{2190}-\x{21FF}\x{2900}-\x{297F}';
    $othernumbers = '\x{2460}-\x{24FF}';
    $geometric = '\x{25A0}-\x{25FF}';
    $emojis = '\x{1F000}-\x{1F6FF}';

    /*
    if (preg_match('/^(\/|https?:|ftp:)/i', $fullopenaire) or preg_match('|^/|', $fullopenaire)) {
        // encode extra chars in URLs - this does not make it always valid, but it helps with some UTF-8 problems
        // Thanks to 💩.la emojis count as valid, too.
        $allowed = "[" . $letters . $latin . $digits . $symbols . $arabic . $math . $othernumbers . $geometric .
            $emojis . "]" . preg_quote(';/?:@=&$_.+!*(),-#%', '/');
        $fullopenaire = preg_replace_callback("/[^$allowed]/u", 'openaire_filter_callback', $fullopenaire);
    } else {
        // encode special chars only
        $fullopenaire = str_replace('"', '%22', $fullopenaire);
        $fullopenaire = str_replace('\'', '%27', $fullopenaire);
        $fullopenaire = str_replace(' ', '%20', $fullopenaire);
        $fullopenaire = str_replace('<', '%3C', $fullopenaire);
        $fullopenaire = str_replace('>', '%3E', $fullopenaire);
    }

    // add variable openaire parameters
    if (!empty($parameters)) {
        if (!$config) {
            $config = get_config('openaire');
        }
        $paramvalues = openaire_get_variable_values($openaire, $cm, $course, $config);

        foreach ($parameters as $parse=>$parameter) {
            if (isset($paramvalues[$parameter])) {
                $parameters[$parse] = rawopenaireencode($parse).'='.rawopenaireencode($paramvalues[$parameter]);
            } else {
                unset($parameters[$parse]);
            }
        }

        if (!empty($parameters)) {
            if (stripos($fullopenaire, 'teamspeak://') === 0) {
                $fullopenaire = $fullopenaire.'?'.implode('?', $parameters);
            } else {
                $join = (strpos($fullopenaire, '?') === false) ? '?' : '&';
                $fullopenaire = $fullopenaire.$join.implode('&', $parameters);
            }
        }
    }

    // encode all & to &amp; entity
    $fullopenaire = str_replace('&', '&amp;', $fullopenaire);
    */

    return $fullopenaire;
}

/**
 * Unicode encoding helper callback
 * @internal
 * @param array $matches
 * @return string
 */
function openaire_filter_callback($matches) {
    return rawopenaireencode($matches[0]);
}

/**
 * Print openaire header.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @return void
 */
function openaire_print_header($openaire, $cm, $course) {
    global $PAGE, $OUTPUT;

    $PAGE->set_title($course->shortname.': '.$openaire->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($openaire);
    echo $OUTPUT->header();
}

/**
 * Print openaire heading.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @param bool $notused This variable is no longer used.
 * @return void
 */
function openaire_print_heading($openaire, $cm, $course, $notused = false) {
    global $OUTPUT;
    echo $OUTPUT->heading(format_string($openaire->name), 2);
}

/**
 * Print openaire introduction.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @param bool $ignoresettings print even if not specified in modedit
 * @return void
 */
function openaire_print_intro($openaire, $cm, $course, $ignoresettings=false) {
    global $OUTPUT;

    $options = empty($openaire->displayoptions) ? array() : unserialize($openaire->displayoptions);
    if ($ignoresettings or !empty($options['printintro'])) {
        if (trim(strip_tags($openaire->intro))) {
            echo $OUTPUT->box_start('mod_introbox', 'openaireintro');
            echo format_module_intro('openaire', $openaire, $cm->id);
            echo $OUTPUT->box_end();
        }
    }
}

/**
 * Display openaire frames.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @return does not return
 */
function openaire_display_frame($openaire, $cm, $course) {
    global $PAGE, $OUTPUT, $CFG;

    $frame = optional_param('frameset', 'main', PARAM_ALPHA);

    if ($frame === 'top') {
        $PAGE->set_pagelayout('frametop');
        openaire_print_header($openaire, $cm, $course);
        openaire_print_heading($openaire, $cm, $course);
        openaire_print_intro($openaire, $cm, $course);
        echo $OUTPUT->footer();
        die;

    } else {
        $config = get_config('openaire');
        $context = context_module::instance($cm->id);
        $exteopenaire = openaire_get_full_url($openaire, $cm, $course, $config);
        $navopenaire = "$CFG->wwwroot/mod/openaire/view.php?id=$cm->id&amp;frameset=top";
        $coursecontext = context_course::instance($course->id);
        $courseshortname = format_string($course->shortname, true, array('context' => $coursecontext));
        $title = strip_tags($courseshortname.': '.format_string($openaire->name));
        $framesize = $config->framesize;
        $modulename = s(get_string('modulename','openaire'));
        $contentframetitle = s(format_string($openaire->name));
        $dir = get_string('thisdirection', 'langconfig');

        $extframe = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html dir="$dir">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>$title</title>
  </head>
  <frameset rows="$framesize,*">
    <frame src="$navopenaire" title="$modulename"/>
    <frame src="$exteopenaire" title="$contentframetitle"/>
  </frameset>
</html>
EOF;

        @header('Content-Type: text/html; charset=utf-8');
        echo $extframe;
        die;
    }
}

/**
 * Print openaire info and link.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @return does not return
 */
function openaire_print_workaround($openaire, $cm, $course) {
    global $OUTPUT;

    openaire_print_header($openaire, $cm, $course);
    openaire_print_heading($openaire, $cm, $course, true);
    openaire_print_intro($openaire, $cm, $course, true);

    $fullopenaire = openaire_get_full_url($openaire, $cm, $course);

    $display = openaire_get_final_display_type($openaire);
    if ($display == RESOURCELIB_DISPLAY_POPUP) {
        $jsfullopenaire = addslashes_js($fullopenaire);
        $options = empty($openaire->displayoptions) ? array() : unserialize($openaire->displayoptions);
        $width  = empty($options['popupwidth'])  ? 620 : $options['popupwidth'];
        $height = empty($options['popupheight']) ? 450 : $options['popupheight'];
        $wh = "width=$width,height=$height,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes";
        $extra = "onclick=\"window.open('$jsfullopenaire', '', '$wh'); return false;\"";

    } else if ($display == RESOURCELIB_DISPLAY_NEW) {
        $extra = "onclick=\"this.target='_blank';\"";

    } else {
        $extra = '';
    }

    echo '<div class="openaireworkaround">';
    //print_string('clicktoopen', 'openaire', "<a href=\"$fullopenaire\" $extra>$fullopenaire</a>");
    echo '</div>';

    echo $OUTPUT->footer();
    die;
}

/**
 * Display embedded openaire file.
 * @param object $openaire
 * @param object $cm
 * @param object $course
 * @return does not return
 */
function openaire_display_embed($openaire, $cm, $course) {
    global $CFG, $PAGE, $OUTPUT;

    $mimetype = resourcelib_guess_url_mimetype($openaire->externalurl);
    $fullopenaire  = openaire_get_full_url($openaire, $cm, $course);
    $title    = $openaire->name;

    $link = html_writer::tag('a', $fullopenaire, array('href'=>str_replace('&amp;', '&', $fullopenaire)));
    $clicktoopen = get_string('clicktoopen', 'openaire', $link);
    $moodleopenaire = new moodle_openaire($fullopenaire);

    $extension = resourcelib_get_extension($openaire->externalurl);

    $mediamanager = core_media_manager::instance($PAGE);
    $embedoptions = array(
        core_media_manager::OPTION_TRUSTED => true,
        core_media_manager::OPTION_BLOCK => true
    );

    if (in_array($mimetype, array('image/gif','image/jpeg','image/png'))) {  // It's an image
        $code = resourcelib_embed_image($fullopenaire, $title);

    } else if ($mediamanager->can_embed_openaire($moodleopenaire, $embedoptions)) {
        // Media (audio/video) file.
        $code = $mediamanager->embed_openaire($moodleopenaire, $title, 0, 0, $embedoptions);

    } else {
        // anything else - just try object tag enlarged as much as possible
        $code = resourcelib_embed_general($fullopenaire, $title, $clicktoopen, $mimetype);
    }

    openaire_print_header($openaire, $cm, $course);
    openaire_print_heading($openaire, $cm, $course);

    echo $code;

    openaire_print_intro($openaire, $cm, $course);

    echo $OUTPUT->footer();
    die;
}

/**
 * Decide the best display format.
 * @param object $openaire
 * @return int display type constant
 */
function openaire_get_final_display_type($openaire) {
    global $CFG;

    if ($openaire->display != RESOURCELIB_DISPLAY_AUTO) {
        return $openaire->display;
    }

    // detect links to local moodle pages
    if (strpos($openaire->externalurl, $CFG->wwwroot) === 0) {
        if (strpos($openaire->externalurl, 'file.php') === false and strpos($openaire->externalurl, '.php') !== false ) {
            // most probably our moodle page with navigation
            return RESOURCELIB_DISPLAY_OPEN;
        }
    }

    static $download = array('application/zip', 'application/x-tar', 'application/g-zip',     // binary formats
                             'application/pdf', 'text/html');  // these are known to cause trouble for external links, sorry
    static $embed    = array('image/gif', 'image/jpeg', 'image/png', 'image/svg+xml',         // images
                             'application/x-shockwave-flash', 'video/x-flv', 'video/x-ms-wm', // video formats
                             'video/quicktime', 'video/mpeg', 'video/mp4',
                             'audio/mp3', 'audio/x-realaudio-plugin', 'x-realaudio-plugin',   // audio formats,
                            );

    $mimetype = resourcelib_guess_url_mimetype($openaire->externalurl);

    if (in_array($mimetype, $download)) {
        return RESOURCELIB_DISPLAY_DOWNLOAD;
    }
    if (in_array($mimetype, $embed)) {
        return RESOURCELIB_DISPLAY_EMBED;
    }

    // let the browser deal with it somehow
    return RESOURCELIB_DISPLAY_OPEN;
}

/**
 * Get the parameters that may be appended to URL
 * @param object $config openaire module config options
 * @return array array describing opt groups
 */
function openaire_get_variable_options($config) {
    global $CFG;

    $options = array();
    $options[''] = array('' => get_string('chooseavariable', 'openaire'));

    $options[get_string('course')] = array(
        'courseid'        => 'id',
        'coursefullname'  => get_string('fullnamecourse'),
        'courseshortname' => get_string('shortnamecourse'),
        'courseidnumber'  => get_string('idnumbercourse'),
        'coursesummary'   => get_string('summary'),
        'courseformat'    => get_string('format'),
    );

    $options[get_string('modulename', 'openaire')] = array(
        'openaireinstance'     => 'id',
        'openairecmid'         => 'cmid',
        'openairename'         => get_string('name'),
        'openaireidnumber'     => get_string('idnumbermod'),
    );

    $options[get_string('miscellaneous')] = array(
        'sitename'        => get_string('fullsitename'),
        'serveropenaire'       => get_string('serveropenaire', 'openaire'),
        'currenttime'     => get_string('time'),
        'lang'            => get_string('language'),
    );
    if (!empty($config->secretphrase)) {
        $options[get_string('miscellaneous')]['encryptedcode'] = get_string('encryptedcode');
    }

    $options[get_string('user')] = array(
        'userid'          => 'id',
        'userusername'    => get_string('username'),
        'useridnumber'    => get_string('idnumber'),
        'userfirstname'   => get_string('firstname'),
        'userlastname'    => get_string('lastname'),
        'userfullname'    => get_string('fullnameuser'),
        'useremail'       => get_string('email'),
        'usericq'         => get_string('icqnumber'),
        'userphone1'      => get_string('phone1'),
        'userphone2'      => get_string('phone2'),
        'userinstitution' => get_string('institution'),
        'userdepartment'  => get_string('department'),
        'useraddress'     => get_string('address'),
        'usercity'        => get_string('city'),
        'usertimezone'    => get_string('timezone'),
        'useropenaire'         => get_string('webpage'),
    );

    if ($config->rolesinparams) {
        $roles = role_fix_names(get_all_roles());
        $roleoptions = array();
        foreach ($roles as $role) {
            $roleoptions['course'.$role->shortname] = get_string('yourwordforx', '', $role->localname);
        }
        $options[get_string('roles')] = $roleoptions;
    }

    return $options;
}

/**
 * Get the parameter values that may be appended to URL
 * @param object $openaire module instance
 * @param object $cm
 * @param object $course
 * @param object $config module config options
 * @return array of parameter values
 */
function openaire_get_variable_values($openaire, $cm, $course, $config) {
    global $USER, $CFG;

    $site = get_site();

    $coursecontext = context_course::instance($course->id);

    $values = array (
        'courseid'        => $course->id,
        'coursefullname'  => format_string($course->fullname, true, array('context' => $coursecontext)),
        'courseshortname' => format_string($course->shortname, true, array('context' => $coursecontext)),
        'courseidnumber'  => $course->idnumber,
        'coursesummary'   => $course->summary,
        'courseformat'    => $course->format,
        'lang'            => current_language(),
        'sitename'        => format_string($site->fullname, true, array('context' => $coursecontext)),
        'serveropenaire'       => $CFG->wwwroot,
        'currenttime'     => time(),
        'openaireinstance'     => $openaire->id,
        'openairecmid'         => $cm->id,
        'openairename'         => format_string($openaire->name, true, array('context' => $coursecontext)),
        'openaireidnumber'     => $cm->idnumber,
    );

    if (isloggedin()) {
        $values['userid']          = $USER->id;
        $values['userusername']    = $USER->username;
        $values['useridnumber']    = $USER->idnumber;
        $values['userfirstname']   = $USER->firstname;
        $values['userlastname']    = $USER->lastname;
        $values['userfullname']    = fullname($USER);
        $values['useremail']       = $USER->email;
        $values['usericq']         = $USER->icq;
        $values['userphone1']      = $USER->phone1;
        $values['userphone2']      = $USER->phone2;
        $values['userinstitution'] = $USER->institution;
        $values['userdepartment']  = $USER->department;
        $values['useraddress']     = $USER->address;
        $values['usercity']        = $USER->city;
        $now = new DateTime('now', core_date::get_user_timezone_object());
        $values['usertimezone']    = $now->getOffset() / 3600.0; // Value in hours for BC.
        $values['useropenaire']         = $USER->openaire;
    }

    // weak imitation of Single-Sign-On, for backwards compatibility only
    // NOTE: login hack is not included in 2.0 any more, new contrib auth plugin
    //       needs to be createed if somebody needs the old functionality!
    if (!empty($config->secretphrase)) {
        $values['encryptedcode'] = openaire_get_encrypted_parameter($openaire, $config);
    }

    //hmm, this is pretty fragile and slow, why do we need it here??
    if ($config->rolesinparams) {
        $coursecontext = context_course::instance($course->id);
        $roles = role_fix_names(get_all_roles($coursecontext), $coursecontext, ROLENAME_ALIAS);
        foreach ($roles as $role) {
            $values['course'.$role->shortname] = $role->localname;
        }
    }

    return $values;
}

/**
 * BC internal function
 * @param object $openaire
 * @param object $config
 * @return string
 */
function openaire_get_encrypted_parameter($openaire, $config) {
    global $CFG;

    if (file_exists("$CFG->dirroot/local/externserverfile.php")) {
        require_once("$CFG->dirroot/local/externserverfile.php");
        if (function_exists('extern_server_file')) {
            return extern_server_file($openaire, $config);
        }
    }
    return md5(getremoteaddr().$config->secretphrase);
}

/**
 * Optimised mimetype detection from general URL
 * @param $fullopenaire
 * @param int $size of the icon.
 * @return string|null mimetype or null when the filetype is not relevant.
 */
function openaire_guess_icon($fullopenaire, $size = null) {
    global $CFG;
    require_once("$CFG->libdir/filelib.php");

    if (substr_count($fullopenaire, '/') < 3 or substr($fullopenaire, -1) === '/') {
        // Most probably default directory - index.php, index.html, etc. Return null because
        // we want to use the default module icon instead of the HTML file icon.
        return null;
    }

    $icon = file_extension_icon($fullopenaire, $size);
    $htmlicon = file_extension_icon('.htm', $size);
    $unknownicon = file_extension_icon('', $size);

    // We do not want to return those icon types, the module icon is more appropriate.
    if ($icon === $unknownicon || $icon === $htmlicon) {
        return null;
    }

    return $icon;
}
