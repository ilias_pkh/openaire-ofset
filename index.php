<?php

require('../../config.php');

$id = required_param('id', PARAM_INT); // course id

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$params = array(
    'context' => context_course::instance($course->id)
);
$event = \mod_openaire\event\course_module_instance_list_viewed::create($params);
$event->add_record_snapshot('course', $course);
$event->trigger();

$stropenaire       = get_string('modulename', 'openaire');
$stropenaires      = get_string('modulenameplural', 'openaire');
$strname         = get_string('name');
$strintro        = get_string('moduleintro');
$strlastmodified = get_string('lastmodified');

$PAGE->set_url('/mod/openaire/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname.': '.$stropenaires);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($stropenaires);
echo $OUTPUT->header();
echo $OUTPUT->heading($stropenaires);

if (!$openaires = get_all_instances_in_course('openaire', $course)) {
    notice(get_string('thereareno', 'moodle', $stropenaires), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($usesections) {
    $strsectionname = get_string('sectionname', 'format_'.$course->format);
    $table->head  = array ($strsectionname, $strname, $strintro);
    $table->align = array ('center', 'left', 'left');
} else {
    $table->head  = array ($strlastmodified, $strname, $strintro);
    $table->align = array ('left', 'left', 'left');
}

$modinfo = get_fast_modinfo($course);
$currentsection = '';
foreach ($openaires as $openaire) {
    $cm = $modinfo->cms[$openaire->coursemodule];
    if ($usesections) {
        $printsection = '';
        if ($openaire->section !== $currentsection) {
            if ($openaire->section) {
                $printsection = get_section_name($course, $openaire->section);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $openaire->section;
        }
    } else {
        $printsection = '<span class="smallinfo">'.userdate($openaire->timemodified)."</span>";
    }

    $extra = empty($cm->extra) ? '' : $cm->extra;
    $icon = '';
    if (!empty($cm->icon)) {
        // each openaire has an icon in 2.0
        $icon = $OUTPUT->pix_icon($cm->icon, get_string('modulename', $cm->modname)) . ' ';
    }

    $class = $openaire->visible ? '' : 'class="dimmed"'; // hidden modules are dimmed
    $table->data[] = array (
        $printsection,
        "<a $class $extra href=\"view.php?id=$cm->id\">".$icon.format_string($openaire->name)."</a>",
        format_module_intro('openaire', $openaire, $cm->id));
}

echo html_writer::table($table);

echo $OUTPUT->footer();
