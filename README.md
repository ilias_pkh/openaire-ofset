# OpenAIRE OFSET

OpenAIRE for the space/geospatial sector (OFSET) is a service that has been developed by Planetek Hellas in the scope of the OpenAIRE - ADVANCE programme. 

## Project introduction

The service aims at integrating the OpenAIRE infrastructure to a dynamic learning platform dedicated to the space/geospatial sector, developed by Planetek within EO4GEO project (http://www.eo4geo.eu/).  The solution aims, in principal, at reaching the space/geospatial community, allowing easy access to a vast amount of scientific and scholarly records, raising at the same time the impact of Open Science and OpenAIRE within this community. The service module, in the form of a plugin, creates a bridge between the OpenAIRE infrastructure and the EO4GEO open educational resources.  Through this plug-in, the users of the EO4GEO platform are able to perform queries and receive filtered and well-structured results that can be easily integrated in the educational platform.

The EO4GEO learning platform is built using Moodle. Moodle is a free and open-source learning management system (LMS) written in PHP and distributed under the GNU General Public License. Developed on pedagogical principles, Moodle is used for blended learning, distance education, flipped classroom and other e-learning projects in schools, universities, workplaces and other sectors. EO4GEO is implemented in a customized version of Moodle. Plugins are used to extend Moodle functionality.

Through the implemented service, users of the EO4GEO platform are able to use an advanced search engine that gives them the chance to request well defined information, according to their requirements. In this way, users receive very well-structured and dedicated to their interest results. The users have also the opportunity to be redirected to the page where their search results are available for further description and download.
The service functions as an OpenAIRE plug-in, a software extension that adds extra features to the EO4GEO learning platform. This extends certain functions within the learning platform, add a new item to the platform’s interface and give the platform additional capabilities. According to the user requests, the plug-in is able to reach OpenAIRE data and provide them to the EO4GEO platform, in a meaningful and well-structured way, allowing users of the platform (tutors and students) to exploit those results in the most optimum.

At the same time, the OpenAIRE OFSET plugin is being developed as a Moodle plugin and is also being available as such. This means that if a Moodle user/owner of a Moodle based platform, other than EO4GEO, would like to use the OpenAIRE plugin that will be possible, following the same installation procedure that is established for all Moodle plugins.

## Installing the OpenAIRE plugin

The OpenAIRE OFSET plugin is provided as a Moodle plugin that is being installed in the EO4GEO learning platform. The integration of the OpenAIRE OFSET plugin into EO4GEO – but also to any other learning management system based on Moodle – follows the installation procedure that is being defined for all other Moodle plugins. In general, Moodle plugins enable the administrators of the learning platform to add additional features and functionality to Moodle, such as new activities, new quiz question types, new reports, integrations with other systems and many more.
To install a plugin, its source code must be put (deployed) into the appropriate location inside the Moodle installation directory and the main administration page Administration > Site administration > Notifications must be visited. The OpenAIRE OFSET plugin can be deployed into Moodle in two ways: by uploading a ZIP file or it may be deployed manually at the server. 

**Installing via uploaded ZIP file**

The web server process has to have write access to the plugin type folder (activity modules and resources) where the new plugin is to be installed in order to use either of these methods.
In order to install the OpenAIRE OFSET plugin into a Moodle installation the following steps have to be followed: 
- Download the OpenAIRE OFSET plugin zip file: [openaire-ofset.zip](https://gitlab.com/pkh-pub/openaire-ofset/-/blob/master/openaire-ofset.zip) from this repository.
- Login to your Moodle site as an admin and go to `Administration > Site administration > Plugins > Install plugins`.
- Upload the ZIP file. You should only be prompted to add extra details (in the Show more section) if your plugin is not automatically detected.
- If your target directory is not writeable, you will see a warning message.
- Check the plugin validation report

 
**Installing manually at the server**

If you can't deploy the plugin code via the administration web interface, you have to copy it to the server file system manually (e.g. if the web server process does not have write access to the Moodle installation tree to do this for you).
- The correct place in the Moodle code tree for our plugin type is:
`/path/to/moodle/mod/ - activity modules and resources`
- Download the OpenAIRE OFSET plugin zip file  [openaire-ofset.zip](https://gitlab.com/pkh-pub/openaire-ofset/-/blob/master/openaire-ofset.zip) from this repository.
- Upload or copy it to your Moodle server. 
- Unzip it under `/path/to/moodle/mod/` - activity modules and resources, which is the right place for this plugin type.
- In your Moodle site (as admin) go to Site administration > Notifications (you should get a message saying the plugin is installed).


The OpenAIRE OFSET plugin has been developed in compliance with the latest Moodle version 3.10.


## Usage
The OpenAIRE plugin is available to the user and can be accessed when adding a new activity or resource in the course as depicted in the screenshots below:

![Access additional features during the creation of a course](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-add-activity.png)

![The OpenAIRE plugin in the list of available features](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-activity-list.png)

By clicking on the OpenAIRE plugin, a form appears, where the user can perform advanced search in OpenAIRE and browse the relevant results. 

![The OpenAIRE form integrated into the course creation](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-add.png)

The form uses a set of the most popular search fields, that the user can set in order to refine the returned results. At the same time the user does not need to set all these input parameters in the form, but only the ones that he considers the most valuable for his specific search. 
The OpenAIRE APIs that are being queried are the following: 
- Publications: http://api.openaire.eu/search/publications
- Datasets: http://api.openaire.eu/search/datasets
- Software: http://api.openaire.eu/search/software
- Other research products metadata: http://api.openaire.eu/search/other

![Advanced search fields](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-search.png)

Once the user has performed an OpenAIRE search, a set of results is returned. The search results are actual links to openAIRE services, that open in a separate tab, and where the user can further consult and download relevant resources.

![OpenAIRE Search Results](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-search-results.png)

Then, the user can select only the results that he considers relevant, by clicking on the corresponding checkbox. 

![OpenAIRE Selected Search Results](https://gitlab.com/pkh/openaire-ofset/-/raw/master/screenshots/openaire-selected-search-results.png)

After this procedure, the selected results are saved in the EO4GEO database and are available to all students that follow the course. 


## Development Instructions

Development Instructions for developping a Moodle plugin can be found in the following links:

https://moodle.com/news/learn-moodle-plugin-development/

https://moodle.com/faq/how-do-i-create-a-moodle-plugin/
