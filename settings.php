<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once("$CFG->libdir/resourcelib.php");

    $displayoptions = resourcelib_get_displayoptions(array(RESOURCELIB_DISPLAY_AUTO,
                                                           RESOURCELIB_DISPLAY_EMBED,
                                                           RESOURCELIB_DISPLAY_FRAME,
                                                           RESOURCELIB_DISPLAY_OPEN,
                                                           RESOURCELIB_DISPLAY_NEW,
                                                           RESOURCELIB_DISPLAY_POPUP,
                                                          ));
    $defaultdisplayoptions = array(RESOURCELIB_DISPLAY_AUTO,
                                   RESOURCELIB_DISPLAY_EMBED,
                                   RESOURCELIB_DISPLAY_OPEN,
                                   RESOURCELIB_DISPLAY_POPUP,
                                  );

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_configtext('openaire/framesize',
        get_string('framesize', 'openaire'), get_string('configframesize', 'openaire'), 130, PARAM_INT));
    $settings->add(new admin_setting_configpasswordunmask('openaire/secretphrase', get_string('password'),
        get_string('configsecretphrase', 'openaire'), ''));
    $settings->add(new admin_setting_configcheckbox('openaire/rolesinparams',
        get_string('rolesinparams', 'openaire'), get_string('configrolesinparams', 'openaire'), false));
    $settings->add(new admin_setting_configmultiselect('openaire/displayoptions',
        get_string('displayoptions', 'openaire'), get_string('configdisplayoptions', 'openaire'),
        $defaultdisplayoptions, $displayoptions));

    //--- modedit defaults -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('openairemodeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_configcheckbox('openaire/printintro',
        get_string('printintro', 'openaire'), get_string('printintroexplain', 'openaire'), 1));
    $settings->add(new admin_setting_configselect('openaire/display',
        get_string('displayselect', 'openaire'), get_string('displayselectexplain', 'openaire'), RESOURCELIB_DISPLAY_AUTO, $displayoptions));
    $settings->add(new admin_setting_configtext('openaire/popupwidth',
        get_string('popupwidth', 'openaire'), get_string('popupwidthexplain', 'openaire'), 620, PARAM_INT, 7));
    $settings->add(new admin_setting_configtext('openaire/popupheight',
        get_string('popupheight', 'openaire'), get_string('popupheightexplain', 'openaire'), 450, PARAM_INT, 7));
}
