<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2020061500;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2020060900;    // Requires this Moodle version
$plugin->component = 'mod_openaire';        // Full name of the plugin (used for diagnostics)
$plugin->cron      = 0;
